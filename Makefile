include base.mk

C_FILES = $(shell find -name "*.c")
H_FILES = $(shell find -name "*.h")

RTFS = $(patsubst %.c, %.rtf, $(C_FILES)) $(patsubst %.h, %.rtf, $(H_FILES))

all: $(RTFS)

formatted:
	mkdir -p formatted

%.rtf: %.c formatted
	$(PYGMENTIZE) -f rtf $< > $@
	mv $@ formatted

%.rtf: %.h formatted
	$(PYGMENTIZE) -f rtf $< > $@
	mv $@ formatted

clean:
	rm -f $(RTFS) formatted/*

venv: $(VENV)

$(VENV): $(PROJECT_DIR)/requirements.txt
	python3 -m venv $(VENV)
	$(PIP) install pygments
