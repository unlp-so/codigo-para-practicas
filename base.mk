PROJECT_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
VENV = $(PROJECT_DIR)/py3-venv
PIP = $(VENV)/bin/pip
PYGMENTIZE = $(VENV)/bin/pygmentize
