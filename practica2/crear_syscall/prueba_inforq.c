#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/syscall.h>

#define NR_rqinfo 462 // Corregir el índice de la syscall

int main (int argc, char **argv)
{
  long ret;
  unsigned long buf[2];
  printf ("invocando syscall ..\n");
  if ((ret = syscall (NR_rqinfo, buf, 2 * sizeof (long))) < 0)
    {
      perror ("ERROR");
      return -1;
    }
  printf ("runnables: %lu\n", buf[0]);
  printf ("uninterruptibles: %lu\n", buf[1]);
  return 0;
}

