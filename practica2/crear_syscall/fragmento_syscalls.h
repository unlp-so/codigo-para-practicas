asmlinkage long sys_old_mmap(struct mmap_arg_struct __user *arg);


/*
 * Not a real system call, but a placeholder for syscalls which are
 * not implemented -- see kernel/sys_ni.c
 */
asmlinkage long sys_ni_syscall(void);

asmlinkage long sys_rqinfo(unsigned long *ubuff, long len);

#endif /* CONFIG_ARCH_HAS_SYSCALL_WRAPPER */
